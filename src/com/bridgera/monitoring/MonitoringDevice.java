package com.bridgera.monitoring;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class MonitoringDevice {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Suman Banerjee\\Desktop\\wizeview\\Wizeview\\Drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		// baseUrl = "https://www.katalon.com/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testMonitoringDevice() throws Exception {
		driver.get("http://wizeviewdev.bridgera.com:8083/login");
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Remember me'])[1]/preceding::input[2]"))
				.clear();
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Remember me'])[1]/preceding::input[2]"))
				.sendKeys("joydeep.misra@bridgera.com");
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Remember me'])[1]/preceding::input[1]"))
				.clear();
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Remember me'])[1]/preceding::input[1]"))
				.sendKeys("test123");
		driver.findElement(By.xpath("//div")).click();
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Remember me'])[1]/preceding::input[1]"))
				.click();
		driver.findElement(By.xpath(
				"(.//*[normalize-space(text()) and normalize-space(.)='Forgot Password?'])[1]/following::button[1]"))
				.click();
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='User'])[1]/following::span[1]"))
				.click();
		// driver.findElement(By.xpath("(.//*[normalize-space(text()) and
		// normalize-space(.)='Action'])[1]/following::input[1]")).click();
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::input[1]"))
				.clear();
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::input[1]"))
				.sendKeys("PRE-2198");
		Thread.sleep(8000);
		driver.findElement(By.cssSelector("svg.svg-inline--fa.fa-eye.fa-w-18")).click();
		driver.findElement(By.xpath("//section[@id='container']/app-header/header/div[3]/div/a/span")).click();
		Thread.sleep(7000);
		try {
			driver.findElement(By.className("pressure-gauge"));
		} catch (Exception e) {
			System.out.println("No Response");
			Email email = new SimpleEmail();
			email.setHostName("smtp.gmail.com");
			email.setSmtpPort(465);
			email.setAuthenticator(new DefaultAuthenticator("iot-automation@bridgera.com","I0t@t3sT"));
			email.setSSLOnConnect(true);
			email.setFrom("rahul.sarkar@bridgera.com");
			email.setSubject("Device not responding");
			email.setMsg("No Response From Device PRE-2198 , Please Check");
			email.addTo("rahul.sarkar@bridgera.com");
			email.send();

			
		}
		
		if (driver.findElement(By.className("pressure-gauge")) != null) {

			System.out.println("Response");
		}
		driver.findElement(By.xpath("//*[@id=\"container\"]/app-header/header/div[3]/div/div/a[3]")).click();

	}

	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
